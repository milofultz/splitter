const splitTable = (paste, columns) => {
  const lines = paste.replace(/\s*$/, '').split('\n');
  const rows = [];
  const linesLength = lines.length;

  for (let line = 0; line < linesLength; line += 1) {
    const row = Math.floor(line / columns);
    const col = line % columns;

    if (col === 0) {
      rows.push([lines[line]]);
    } else {
      rows[row].push(lines[line]);
    }
  }
  return rows.map((row) => row.join('\t')).join('\n');
};
