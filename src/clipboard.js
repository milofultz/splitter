document.addEventListener('paste', (e) => {
  e.preventDefault();
  const paste = e.clipboardData.getData('text/html');
  const cleanedHTML = cleanTableHTML(HTML);
  const columns = +document.getElementById('columns').value;
  const cleanedTable = splitTable(cleanedHTML, columns);
  document.getElementById('output').value = cleanedTable;
  navigator.clipboard.writeText(cleanedTable);
  const message = document.getElementById('message');
  message.textContent = 'The cleaned HTML is on your clipboard.';
  message.style.display = 'block';
  setTimeout(() => {
    message.textContent = '';
    message.style.display = 'none';
  }, 5000);
});
