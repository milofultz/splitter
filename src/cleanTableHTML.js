const cleanTableHTML = (pastedHTML) => {
  const sandbox = document.createElement('div');
  sandbox.id = 'sandbox';
  sandbox.insertAdjacentHTML('afterbegin', pastedHTML);
  sandbox.querySelectorAll('*').forEach((el) => {
    el.removeAttribute('role');
    el.removeAttribute('style');
    el.removeAttribute('dir');
    el.removeAttribute('id');
  });
  sandbox.querySelectorAll('span').forEach((el) => {
    if (!el.parentNode) return;

    while (el.childNodes.length) {
      el.parentNode.insertBefore(el.childNodes[0], el);
    }

    el.remove();
  });

  const cells = Array.from(sandbox.querySelectorAll('p')).map(
    (cell) => cell.innerHTML,
  );

  return cells.join('\n');
};
