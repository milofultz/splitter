/* eslint-env jasmine */
/* eslint-disable no-undef */

describe('splitTable', () => {
  it('should make cells separated by tab', () => {
    const input = 'one\ntwo\nthree';
    const output = 'one\ttwo\tthree';
    expect(splitTable(input, 100)).toBe(output);
  });

  it('should make rows separated by newline at interval', () => {
    const exampleInputs = [
      [1, 'one\ntwo\nthree', 'one\ntwo\nthree'],
      [3, 'one\ntwo\nthree\nfour', 'one\ttwo\tthree\nfour'],
    ];
    exampleInputs.forEach((data) => {
      const [columns, input, output] = data;
      expect(splitTable(input, columns)).toBe(output);
    });
  });
});
